import { Injectable } from '@angular/core';
import { UserModel } from './../models/user.model';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class UserService {
  user: UserModel = new UserModel();
  subject = new BehaviorSubject(this.user);
  constructor() {}
  setUserModel(obj: UserModel) {
    this.user = obj;
    this.subject.next(this.user);
  }
  getUserModel() {
    return this.subject.asObservable();
  }
}
