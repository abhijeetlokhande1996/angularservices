import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../services/user.service';
import { UserModel } from '../models/user.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-c2',
  templateUrl: './c2.component.html',
  styleUrls: ['./c2.component.css'],
})
export class C2Component implements OnInit, OnDestroy {
  user: UserModel;
  sub: Subscription;
  constructor(private user_service: UserService) {}

  ngOnInit(): void {
    this.sub = this.user_service.getUserModel().subscribe((resp: UserModel) => {
      console.log(resp);
      this.user = resp;
    });
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
