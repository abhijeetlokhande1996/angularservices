import { Directive, HostListener, ElementRef } from '@angular/core';
import { NgModel } from '@angular/forms';

@Directive({
  selector: '[appTwoDecimal]'
})
export class TwoDecimalDirective {

  constructor(private el: ElementRef, private ngModel: NgModel) { }

  @HostListener('focus')
  onFocus(){
    this.el.nativeElement.value = this.ngModel.value;
  }

  @HostListener('blur',['$event.target.value'])
  onBlur(orgValue){
    this.el.nativeElement.value = (Math.round(orgValue * 100) /100)
  }

}
