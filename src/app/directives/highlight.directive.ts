import {
  Directive,
  ElementRef,
  Input,
  OnInit,
  AfterViewInit,
  HostListener,
} from '@angular/core';

@Directive({
  selector: '[appHighlight]',
})
export class HighlightDirective implements OnInit, AfterViewInit {
  //@Input('bgColor') bgColor: string;
  @Input('appHighlight') bgColor: string;
  constructor(private el: ElementRef) {}

  ngOnInit(): void {}
  /**
   * Home work
   */
  ngAfterViewInit(): void {
    console.log('View Init Directive');
  }

  @HostListener('mouseenter')
  onMouseEnter() {
    (this.el.nativeElement as HTMLElement).style.backgroundColor = this.bgColor;
  }
  @HostListener('mouseleave')
  onMouseLeave() {
    (this.el.nativeElement as HTMLElement).style.backgroundColor = null;
  }
}
