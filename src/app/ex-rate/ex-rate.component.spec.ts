import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExRateComponent } from './ex-rate.component';

describe('ExRateComponent', () => {
  let component: ExRateComponent;
  let fixture: ComponentFixture<ExRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
