import { Component, OnInit } from '@angular/core';
import { UserModel } from './../models/user.model';
import { UserService } from './../services/user.service';
@Component({
  selector: 'app-c1',
  templateUrl: './c1.component.html',
  styleUrls: ['./c1.component.css'],
})
export class C1Component implements OnInit {
  username: string = null;
  password: string = null;

  constructor(private user_service: UserService) {}

  ngOnInit(): void {}
  sendDataToC2() {
    const user = new UserModel();
    user.username = this.username;
    user.password = this.password;
    this.user_service.setUserModel(user);
  }
}
